// WelcomeScreen.js

import React, { useState } from "react";
import { View, Text } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

function WelcomeScreen () {
    const [name, setName] = useState('');
    const [age, setAge] = useState('');
    const [weight, setWeight] = useState('');
    const [height, setHeight] = useState('');
    const [vegan, setVegan] = useState('');
    const [allergy, setAllergy] = useState('');

    const handleSubmit = async () => {
        // Logic to handle profile data submission
        try {
            const userProfile = { name, age, weight, height, vegan, allergy };
            const jsonValue = JSON.stringify(userProfile);
            await AsyncStorage.setItem('userProfile', jsonValue);
            alert('Profile saved successfully!');
        } catch (error) {
            // Saving error
            alert('Failed to save the profile.');
        }
    };

    return (
        <View>
            <Text>WelcomeScreen</Text>
        </View>
    );

}

export default WelcomeScreen;