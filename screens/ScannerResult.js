// ScannerResult.js

import React, { useEffect, useState } from "react";
import { Alert, StyleSheet, Text, View, Image } from "react-native";
import colors from "../config/colors";

function ScannerResult({ route }) {
    console.log("MONTADO")
    const [name, setName] = useState("");
    const [image, setImage] = useState(false);
    const [nutrition, setNutrition] = useState("");
    const code = route.params.code;

    useEffect(() => {
        
        let isMounted = true; // Agregar bandera para verificar el montaje
    
        const fetchProduct = async () => {
            try {
                const response = await fetch(`https://world.openfoodfacts.org/api/v2/product/${code}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });
                if (!response.ok) {
                    Alert.alert("API Failed");
                    return;
                }
                const data = await response.json();
                if(isMounted){ // Verificar si el componente sigue montado
                    setName(data.product.product_name_es);
                    setImage(data.product.image_url);
                    setNutrition(data.product.nutrition_grades);
                }
            } catch (error) {
                console.error('There has been a problem with your fetch operation:', error);
            }
        };
    
        fetchProduct();
        
        return () => {
            isMounted = false; // Limpiar bandera al desmontar
        };
    }, [code]); // Dependencia: se ejecuta de nuevo si `code` cambia.
    

    return (
        <View style={styles.container}>
            {image && <Image source={{ uri: image }} style={styles.image} resizeMode="contain" />}
            <Text style={styles.title}>{name}</Text>
            <Text style={styles.nutritionGrade}>Valor nutricional: {nutrition.toUpperCase()}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '90%',
        height: 500,
        margin: 50,
        padding: 20,
        alignSelf: 'center',
    },
    title: {
        color: colors.text,
        fontSize: 30,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 50,
    },
    image: {
        width: '100%',
        height: 300,
        alignSelf: 'center',
    },
    nutritionGrade: {
        color: colors.text,
        textAlign: 'center',
        fontSize: 25
    }
});

export default React.memo(ScannerResult);
