// HomePage.js

import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import Header from "../components/header";
import Browser from "../components/browser";
import NewsCard from "../components/newsCard";
import FoodFilter from "../components/foodFilter";
import ClassicButton from "../components/classicButton";

function HomePage({ navigation }) {
    return (
        <ScrollView style={styles.container}>
            <Header />
            <Browser 
                placeholder="Buscar..." 
                style={styles.browser} 
            />
            <NewsCard 
                text="Genera tu cesta y ahorra tiempo!"
                style={styles.newsCard}
            />
            <FoodFilter />
            <ClassicButton handlePress={() => {navigation.navigate('Questions')}} text="Crear lista de la compra"/>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    browser: {
        width: '80%'
    },
    newsCard: {
        margin: 20
    }
});

export default HomePage;
