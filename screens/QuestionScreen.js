// QuestionScreen.js

import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from "react-native";
import colors from "../config/colors";
import Header from "../components/header";

const questions = [
  {
      title: '¿Cuál es tu objetivo con EazyFood?',
      options: ['Dieta equilibrada', 'Ganar músculo', 'Dieta libre', 'Perder peso'],
  },
  {
      "title": "¿Cuáles son tus patologías?",
      "options": ["Colesterol","Anemia","Diabetes","Hipertensión","Ninguna"]
  },
  {
      "title": "¿Cuáles son tus preferencias alimenticias?",
      "options": ["Vegano","Vegetariano","Ecológico","Sin lactosa","Sin huevo","Sin frutos secos","Ninguna"]
  },
  {
      "title": "¿Cuánto quieres gastar en la compra semanalmente?",
      "options": ["Menos de 15€","20-50","50-70","Más de 70"]
  },
  {
    "title": "¿Qué supermercados frecuentas?",
    "options": ["Comercio Local","Mercadona","Dia","Lidl","Alcampo"]
}
]

function QuestionScreen ({ navigation }) {
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [selectedIndex, setSelectedIndex] = useState(0);

    const handleSelect = (key) => {
      setSelectedIndex(key);
    }

    const handleNextPress = () => {
        if (currentQuestionIndex < questions.length - 1) {
            setCurrentQuestionIndex(currentQuestionIndex + 1);
        } else {
            // Fin de cuestionario
            navigation.navigate("List");
        }
    }

    const question = questions[currentQuestionIndex];

    return (
        <ScrollView>
            <Header />
            <View style={styles.container}>
              <Text style={styles.title}>{question.title}</Text>
              {question.options.map((option, index) => (
                  <TouchableOpacity 
                    onPress={() => {handleSelect(index)}} 
                    key={index} style={[
                    styles.optionButton, 
                    (selectedIndex === index) ? styles.selected : null]}>
                    <Text style={styles.optionText}>{option}</Text>
                  </TouchableOpacity>
              ))}
              <TouchableOpacity style={styles.nextButton} onPress={handleNextPress}>
                  <Text style={styles.nextButtonText}>Siguiente</Text>
              </TouchableOpacity>
            </View>
        </ScrollView>
    );

}

const styles = StyleSheet.create({
    container: {
      width: '90%',
      alignSelf: 'center',
      paddingBottom: 50
    },
    selected: {
      backgroundColor: 'rgba(54, 181, 76, 0.6)',
      borderWidth: 1,
      borderColor: colors.primary
    },
    title: {
      fontSize: 30,
      fontWeight: '900',
      marginBottom: 20,
      textAlign: 'center', // Centra el título
      color: colors.secondary
    },
    optionButton: {
      paddingVertical: 12,
      paddingHorizontal: 15,
      borderRadius: 25,
      borderWidth: 1,
      borderColor: '#E0E0E0',
      backgroundColor: '#FAFAFA',
      marginBottom: 10,
      height: 75,
      justifyContent: 'center'
    },
    optionText: {
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'center', // Centra el texto de la opción
      color: colors.text,
    },
    nextButton: {
      backgroundColor: '#36B54C', // Color de tu botón
      paddingVertical: 12,
      borderRadius: 25,
      marginTop: 20,
    },
    nextButtonText: {
      color: 'white',
      textAlign: 'center',
      fontSize: 18,
    },
  });  

export default QuestionScreen;