// ScannerScreen.js

import React, { useEffect, useState, useCallback } from "react";
import { View, StyleSheet } from "react-native";
import { useCameraDevice, Camera, useCodeScanner } from "react-native-vision-camera";
import { useFocusEffect } from '@react-navigation/native';

function ScannerScreen ({ navigation }) {
    console.log("Scaner montado")
    const device = useCameraDevice('back');
    const [isScanning, setIsScanning] = useState(true);

    useFocusEffect(
        useCallback(() => {
            setIsScanning(true);
            return () => setIsScanning(false); // Opcional, dependiendo de la lógica deseada.
        }, [])
    );
    

    const codeScanner = useCodeScanner({
        codeTypes: ['qr', 'ean-13'],
        onCodeScanned: (codes) => {
            try {
                if (isScanning) {
                    const codeResult = codes[0].value;
                    setIsScanning(false); // Detiene el escaneo después del primer código detectado
                    console.log(`Scanned ${codeResult} codes!`)
                    navigation.navigate("Result", {code: codeResult});
                }

            } catch (error) {
                console.log(error);
            }
        }
    })

    if (device == null) return (<View></View>);
    return (
        <Camera
        style={StyleSheet.absoluteFill}
        device={device}
        isActive={isScanning}
        codeScanner={codeScanner}
        />
    )
}

export default ScannerScreen;