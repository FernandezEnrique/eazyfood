//ListScreen.js

import React from "react";
import { ScrollView, View, Text, StyleSheet } from "react-native";
import list from "../data/list";
import DailyMenu from "../components/dailyMenu";
import Header from "../components/header";
import colors from "../config/colors";

function ListScreen() {
  return (
    <ScrollView style={styles.container}>
      <Header />
      <Text style={{
        // textAlign: 'center',
        marginLeft: 20,
        color: colors.text,
        fontSize: 25,
        fontWeight: 'bold'
      }}>Menú Semanal</Text>
      {list.map((item, index) => (
        <View key={index} style={styles.dayContainer}>
          <Text style={styles.dayTitle}>{item.day}</Text>

          {/* Desayuno */}
          <ScrollView 
            style={styles.mealContainer}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          >
            <DailyMenu 
              title={item.breakfast.name} 
              image={item.breakfast.image}
              price={item.breakfast.price}
              products={item.breakfast.products}
            />
            <DailyMenu 
              title={item.lunch.name} 
              image={item.lunch.image}
              price={item.lunch.price}
              products={item.lunch.products}
            />
            <DailyMenu 
              title={item.dinner.name} 
              image={item.dinner.image}
              price={item.dinner.price}
              products={item.dinner.products}
            />
          </ScrollView>

        </View>
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  dayContainer: {
    marginVertical: 8,
    padding: 10,
    backgroundColor: '#ffffff', // Fondo blanco para cada día
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  dayTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333', // Texto oscuro para el título del día
    marginBottom: 8,
    textAlign: 'center'
  },
  mealContainer: {
    marginBottom: 8,
  },
  mealTitle: {
    fontSize: 16,
    fontWeight: '600',
    color: '#666666', // Texto más suave para el título de la comida
    marginBottom: 4,
  },
  menu: {
    fontSize: 14,
    color: '#333333', // Texto oscuro para la descripción del menú
  },
  productContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 4,
  },
  productImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  productName: {
    fontSize: 14,
    fontWeight: '500',
    color: '#333',
  },
  productPrice: {
    fontSize: 14,
    color: '#666',
    marginLeft: 'auto',
  },
});

export default ListScreen;
