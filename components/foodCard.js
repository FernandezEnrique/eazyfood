// foodCard.js

import React from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";

function FoodCard ({ title, imagePath, containerStyles, isActive, handlePress }) {

    return (
        <TouchableOpacity onPress={handlePress} style={[styles.container, containerStyles, isActive ? styles.isActive : null]}>
            <Text style={[styles.text, isActive ? styles.isActiveText : null]}>{title}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '40%',
        height: 40,
        backgroundColor: '#fff',
        padding: 10,
        margin: 10,
        borderRadius: 10,
    },
    text: {
        color: '#000',
        alignSelf: 'center',
    },
    isActive: {
        backgroundColor: '#000',
    },
    isActiveText: {
        color: '#fff',
    }
})

export default FoodCard;