// foodFilter.js

import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import FoodCard from "./foodCard";
import FoodScroller from "./foodScroller"; 

function FoodFilter () {
    // State to change the title of the active FoodCard
    const [activeCard, setActiveCard] = useState("Todo");
    const [index, setIndex] = useState(1);

    // Function to handle FoodCard click
    const handlePress = (title, key) => {
        setActiveCard(title);
        setIndex(key);
    }

    return (
        <View style={styles.container}>
            <View style={styles.FoodCardContainer}>
                <FoodCard 
                    title="Bajada de Precio"
                    isActive={activeCard === "Todo"}
                    handlePress={() => {handlePress("Todo", 1)}}
                />
                <FoodCard 
                    title="Novedades"
                    isActive={activeCard === "Novedades"}
                    handlePress={() => {handlePress("Novedades", 2)}}
                />
            </View>
            <FoodScroller index={index}/>
        </View>
    );

}

const styles = StyleSheet.create({
    container: {

    },
    FoodCardContainer: {
        flexDirection: 'row',
        alignSelf: 'center'
    }
})

export default FoodFilter;