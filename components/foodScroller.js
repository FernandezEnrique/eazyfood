// foodScroller.js

import React, { useEffect, useState } from "react";
import { ScrollView, View, Text, StyleSheet } from "react-native";
import apiService from "../class/ApiService";
import ItemCard from "./itemCard";
import { list1, list2 } from "../data/data";

function FoodScroller({ index }) {
    const [data1, setData1] = useState(list1);
    const [data2, setData2] = useState(list2);
    
    return (
        <View>
            {index === 1 && (
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={true}
                    style={styles.scrollView}
                >
                    {data1.map((item, index) => (
                        <ItemCard
                            key={index}
                            name={item.name}
                            price={item.price}
                            image={item.image}
                        />
                    ))}
                </ScrollView>
            )}

            {index === 2 && (
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={true}
                    style={styles.scrollView}
                >
                    {data2.map((item, index) => (
                        <ItemCard
                            key={index}
                            name={item.name}
                            price={item.price}
                            image={item.image}
                        />
                    ))}
                </ScrollView>

            )}

        </View>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        width: '100%',
        height: 300,
        alignSelf: 'center',
        marginBottom: 10
    }
})

export default FoodScroller;