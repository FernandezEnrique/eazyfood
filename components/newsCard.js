import React from "react";
import { View, Image, StyleSheet, Text } from 'react-native';
import colors from "../config/colors";

function NewsCard ({ style, text, textStyles }) {
    return (
        <View style={[styles.container, style]}>
            <Image
                source={require('../img/newsImg.png')}
                style={styles.image}
            />
            <Text style={[styles.text, textStyles]}>{text}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignContent: 'center',
        padding: 10,
        width: '90%',
        height: 150,
        alignSelf: 'center',
        backgroundColor: 'white',
        borderRadius: 20,
    },
    text: {
        flexShrink: 1,
        color: colors.text,
        fontWeight: '900',
        fontSize: 22,
        color: colors.primary,
        textAlign: 'center',
        alignSelf:'center'
    },
    image: {
        width: 120,
        height: 120,
        marginRight: 20,
    }
});

export default NewsCard;