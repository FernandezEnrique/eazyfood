// ItemCard.js

import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import colors from '../config/colors';

const ItemCard = ({ name, price, image }) => {
  return (
    <View style={styles.card}>
      <Image source={{ uri: image }} style={styles.image} />
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.price}>{price}€/unidad</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    margin: 10,
    width: 200,
    height: 250,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  image: {
    width: '100%',
    height: '60%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  name: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.text,
    textAlign: 'center'
  },
  price: {
    fontSize: 14,
    color: colors.text
  },
});

export default ItemCard;
