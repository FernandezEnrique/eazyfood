// Header.js

import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import colors from '../config/colors';

function Header() {
  return (
    <View style={styles.headerContainer}>
      <Image
        source={require('../img/logo_header.png')} // Asegúrate de cambiar esto por el camino correcto de tu imagen
        style={styles.logo}
      />
    </View>
  );
}

const styles = StyleSheet.create({
    headerContainer: {
      flexDirection: 'row',
      height: 100,
      alignItems: 'center',
      // justifyContent: 'flex-start' ya no es necesario si solo hay un logo
      paddingHorizontal: 15,
      backgroundColor: colors.background,
    },
    logo: {
      // Establece el ancho y altura deseados para tu logo
      width: 100, // Ajusta esto según el tamaño de tu logo
      height: '100%', // Ajusta esto según el tamaño de tu logo
      resizeMode: 'contain',
    },
  });

export default Header;
