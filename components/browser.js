import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import colors from "../config/colors";

function Browser ({ placeholder, style}) {
    return (
        <View style={[styles.container, style]}>
            <TextInput 
                style={styles.input}
                placeholder={placeholder}
                placeholderTextColor={colors.text}
            />
            <Ionicons name="search" size={20} color="#000" style={styles.icon} />
        </View>
    );

}

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#000',
      borderRadius: 10,
      padding: 10,
      alignSelf: 'center',
    },
    input: {
      flex: 1, // Asegura que el input ocupe todo el espacio menos el del icono
      padding: 0, // Elimina el padding predeterminado para alinear correctamente el texto
      color: colors.text,
    },
    icon: {
      marginLeft: 10, // Añade espacio entre el input y el icono
    },
  });

export default Browser;