// classicButton.js

import React from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import colors from "../config/colors";

function ClassicButton ({ text, handlePress}) {
    return (
        <TouchableOpacity onPress={handlePress} style={styles.button}>
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    button: {
        width: '90%',
        height: 50,
        backgroundColor: colors.secondary,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 30,
    },
    text: {
        color: colors.textSecondary,
        fontWeight: '900',
        fontSize: 20,
        alignSelf: 'center'
    }
})

export default ClassicButton;