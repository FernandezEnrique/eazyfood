// dailyMenu.js

import React from "react";
import { Image, StyleSheet, View, Text } from "react-native";
import colors from "../config/colors";

function DailyMenu ({ title, image, price, products }) {
    return (
        <View style={styles.container}>
            <Image 
                source={{ uri: image}}
                style={styles.image}
            />
            <View style={styles.productsContainer}>
                <Text style={styles.title}>{title}</Text>
                <View style={styles.hr}></View>
                {products.map((item, index) => (
                    <Text key={index} style={styles.product}>- {item.name}</Text>
                ))}
                <View style={styles.hr}></View>
                <Text style={styles.price}>{price}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: 350,
        height: 200,
        borderRadius: 20,
        flexDirection: 'row',
        padding: 10,
    },
    title: {
        color: colors.text,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    hr: {
        margin: 5,
        height: 1,
        width: '90%',
        backgroundColor: 'black',
        alignSelf: 'center'
    },
    productsContainer: {
        width: 150,
        height: 150,
        alignSelf: 'center'
    },
    price: {
        color: colors.text,
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 15,
    },
    image: {
        width: 150,
        height: 150,
        alignSelf: 'center',
        borderRadius: 20,
        marginRight: 20,
    },
    product: {
        color: colors.text,
    },
})

export default DailyMenu;