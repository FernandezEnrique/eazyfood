// config/colors.js

export default {
  primary: '#46841c',
  secondary: '#36B54C',
  background: '#fff6f2',
  text: '#000',
  textSecondary: '#fff',
};