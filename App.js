// App.js

import React from 'react';
import HomePage from './screens/HomePage';
import QuestionScreen from './screens/QuestionScreen';
import ListScreen from './screens/ListScreen';
import ScannerResult from './screens/ScannerResult';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import colors from './config/colors';
import screenNames from './config/screenNames';
import ScannerScreen from './screens/ScannerScreen';

const Tab = createBottomTabNavigator();

function App() {

  return (
    <NavigationContainer theme={MyTheme}>
      <Tab.Navigator
        initialRouteName='Home'
        screenOptions={{
          headerShown: false,
        }}
      >
        <Tab.Screen 
          name="Home" 
          component={HomePage} 
          options={{  
            headerShown: false,
            tabBarLabel: screenNames.home,
            tabBarIcon: ({ color, size }) => (
              <Ionicons name="home" color={color} size={size} />
            )
          }}
        />
        {/* <Tab.Screen 
          name="Scanner"
          component={ScannerScreen}
          options={{
            headerShown: false,
            tabBarLabel: screenNames.scanner,
            tabBarIcon: ({ color, size }) => (
              <Ionicons name="scan" color={color} size={size} />
            )
          }}
        /> */}
        <Tab.Screen 
          name="Result"
          component={ScannerResult}
          options={{
            headerShown: false,
            tabBarButton: () => null,
          }}
        />
        <Tab.Screen
          name="Questions" 
          component={QuestionScreen}
          options={{
            headerShown: false,
            tabBarButton: () => null,
          }}
        />
        <Tab.Screen
          name="List"
          component={ListScreen}
          options={{
            headerShown: false,
            tabBarButton: () => null,
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );

}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: colors.background,
    primary: colors.primary,
    card: colors.background,
    text: colors.text,
  }
}

export default App;
