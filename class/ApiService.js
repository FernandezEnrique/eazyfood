// ApiService.js
import axios from "axios";
import apiConfig from "../config/apiConfig";
import data from "../data/data";

class ApiService {
    constructor() {
        // Create an axios client with a base URL for the API
        this.mercadonaAPI = axios.create({
            baseURL: apiConfig.MERCADONA_API
        });
    }

    // Method for making GET requests to a given endpoint with optional parameters
    async get(endpoint, params = {}) {
        try {
            // Perform the GET request using the provided endpoint and parameters
            const response = await this.mercadonaAPI.get(endpoint, { params });
            // Return the data from the response
            return response.data;
            // return data;
        } catch (error) {
            // Return the data from the response
            console.error("Error making the GET request:", error);
            throw error;
        }
    }
}

export default new ApiService();