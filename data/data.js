//data.js

export const list1 = [
    {    
        "name": "Naranjas",
        "price": 4.95,
        "image":"https://i.imgur.com/XjSgoZQ.jpeg"
    },
    {
        "name": "Pan de molde cereales",
        "price": 2.90,
        "image":"https://i.imgur.com/j623Tam.jpeg"
    },
    {
        "name": "Espárrago verde fino",
        "price": 1.99,
        "image":"https://i.imgur.com/YWBDuyS.jpeg"
    },
    {
        "name": "Berberechos al natural",
        "price": 3.60,
        "image":"https://i.imgur.com/IWwR24P.jpeg"
    }
]

export const list2 = [
    {
        "name": "Pollo teriyaki",
        "price": 5,
        "image":"https://i.imgur.com/MBu5zzb.jpeg"
    },
    {
        "name": "Pechuga de pavo 0% m.g Hacendado al corte",
        "price": 2.09,
        "image":"https://i.imgur.com/5oLzDAx.jpeg"
    },
    {
        "name": "Mini cereales rellenos de leche Hacendado",
        "price": 1.90,
        "image":"https://i.imgur.com/AdUeXmJ.jpeg"
    },
    {
        "name": "Risotto de setas Hacendado ultracongelados",
        "price": 2.00,
        "image":"https://i.imgur.com/okzmrQ5.jpeg"
    }
]

