// list.js

export default [
    {
      "day": "Lunes",
      "breakfast": {
        "name": "Tostada con aceite y té verde",
        "image": "https://i.imgur.com/AEon10i.jpeg",
        "price": "1.95€",
        "products": [
          {
            "name": "Aceite de oliva virgen extra Hacendado",
            "price": "2.35€"
          },
          {
            "name": "Sal marina en escamas Polasal",
            "price": "1.95€"
          },
          {
            "name": "Té verde Hacendado",
            "price": "Supuesto 1.50€"
          }
        ]
      },
      "lunch": {
        "name": "Ensalada mediterránea",
        "image": "https://i.imgur.com/4ymPjr4.jpeg",
        "price": "2.55€",
        "products": [
          {
            "name": "Aceitunas estilo caseras Hacendado",
            "price": "Supuesto 2.50€"
          },
          {
            "name": "Aceite de oliva virgen extra Hacendado",
            "price": "2.35€"
          },
          {
            "name": "Vinagre balsámico de Módena Hacendado",
            "price": "1.30€"
          }
        ]
      },
      "dinner": {
        "name": "Tortilla española",
        "image": "https://i.imgur.com/V7T2M7v.jpeg",
        "price": "1.2€",
        "products": [
          {
            "name": "Pimienta negra molida Hacendado",
            "price": "0.69€"
          }
        ]
      }
    },
    {
      "day": "Martes",
      "breakfast": {
        "name": "Yogur natural con miel y semillas",
        "image": "https://i.imgur.com/cEg51Dx.png",
        "price": "0.5€",
        "products": [
          {
            "name": "Canela molida Hacendado",
            "price": "0.51€"
          }
        ]
      },
      "lunch": {
        "name": "Quinoa con verduras",
        "image": "https://i.imgur.com/RBoecq3.jpeg",
        "price": "1.85€",
        "products": [
          {
            "name": "Pimentón dulce Hacendado",
            "price": "0.55€"
          },
          {
            "name": "Cúrcuma Hacendado",
            "price": "1.15€"
          }
        ]
      },
      "dinner": {
        "name": "Sopa de verduras",
        "image": "https://i.imgur.com/CeLOynL.jpeg",
        "price": "3.27€",
        "products": [
          {
            "name": "Aceite de oliva virgen extra Hacendado",
            "price": "2.35€"
          },
          {
            "name": "Sal rosa del Himalaya Hacendado",
            "price": "1.95€"
          }
        ]
      }
    },
    {
        "day": "Miércoles",
        "breakfast": {
        "name": "Batido de frutas con agua de coco",
        "image": "https://i.imgur.com/QUAEo5E.jpeg",
        "price": "5.2€",
        "products": [
            {
            "name": "Agua de coco Chaokoh 100% natural",
            "price": "2.60€"
            },
            {
            "name": "Vainilla en rama Hacendado",
            "price": "2.50€"
            }
        ]
        },
        "lunch": {
        "name": "Pasta integral al pesto",
        "image": "https://i.imgur.com/0LMZZAa.jpeg",
        "price": "3.1€",
        "products": [
            {
            "name": "Aceite de oliva virgen extra Hacendado",
            "price": "2.35€"
            },
            {
            "name": "Ajo granulado Hacendado",
            "price": "1.10€"
            },
            {
            "name": "Albahaca Hacendado",
            "price": "0.65€"
            }
        ]
        },
        "dinner": {
        "name": "Ensalada de espinacas y fresas",
        "image": "https://i.imgur.com/ryryRxe.jpeg",
        "price": "3.2€",
        "products": [
            {
            "name": "Vinagre balsámico de Módena Hacendado",
            "price": "1.30€"
            },
            {
            "name": "Aceite de oliva virgen extra Hacendado",
            "price": "2.35€"
            }
        ]
        }
    },
    {
        "day": "Jueves",
        "breakfast": {
        "name": "Pan integral con tomate y ajo",
        "image": "https://i.imgur.com/wKPPwsi.jpeg",
        "price": "4.2€",
        "products": [
            {
            "name": "Aceite de oliva virgen Hacendado",
            "price": "3.20€"
            },
            {
            "name": "Ajo granulado Hacendado",
            "price": "1.10€"
            }
        ]
        },
        "lunch": {
        "name": "Lentejas estofadas con verduras",
        "image": "https://i.imgur.com/HAVNuOQ.jpeg",
        "price": "3.86€",
        "products": [
            {
            "name": "Comino molido Hacendado",
            "price": "0.63€"
            },
            {
            "name": "Aceite de oliva virgen Hacendado",
            "price": "3.20€"
            }
        ]
        },
        "dinner": {
        "name": "Crema de calabaza con nuez moscada",
        "image": "https://i.imgur.com/M1o2Io6.jpeg",
        "price": "2.56€",
        "products": [
            {
            "name": "Nuez moscada molida Hacendado",
            "price": "1.10€"
            }
        ]
        }
    },
    {
        "day": "Viernes",
        "breakfast": {
        "name": "Avena con leche y vinagre de manzana",
        "image": "https://i.imgur.com/Kvgt4Vk.jpeg",
        "price": "1.88€",
        "products": [
            {
            "name": "Nuez moscada molida Hacendado",
            "price": "1.10€"
            },
            {
            "name": "Vinagre de manzana Hacendado",
            "price": "0.69€"
            }
        ]
        },
        "lunch": {
        "name": "Arroz integral con curry",
        "image": "https://i.imgur.com/0W7eohW.jpeg",
        "price": "5.66€",
        "products": [
            {
            "name": "Aceite de coco virgen extra Nat Sanno",
            "price": "4.50€"
            },
            {
            "name": "Curry Hacendado",
            "price": "0.62€"
            }
        ]
        },
        "dinner": {
        "name": "Pimientos rellenos de quinoa",
        "image": "https://i.imgur.com/y4CC7i2.jpeg",
        "price": "0.66€",
        "products": [
            {
            "name": "Hierbas provenzales Hacendado",
            "price": "0.70€"
            }
        ]
        }
    },
    {
        "day": "Sábado",
        "breakfast": {
          "name": "Huevos revueltos con hierbas",
          "image": "https://i.imgur.com/2NGpjXJ.jpeg",
          "price": "2.54€",
          "products": [
            {
              "name": "Hierbas provenzales Hacendado",
              "price": "0.70€"
            }
          ]
        },
        "lunch": {
          "name": "Gazpacho andaluz",
          "image": "https://i.imgur.com/kO8lNbC.jpeg",
          "price": "2.04€",
          "products": [
            {
              "name": "Vinagre de Jerez reserva Hacendado",
              "price": "1.25€"
            }
          ]
        },
        "dinner": {
          "name": "Salmón al horno con eneldo",
          "image": "https://i.imgur.com/D8re88a.jpeg",
          "price": "3.56€",
          "products": [
            {
              "name": "Eneldo Hacendado",
              "price": "1.85€"
            }
          ]
        }
    },
    {
        "day": "Domingo",
        "breakfast": {
          "name": "Crepes caseros con fruta fresca",
          "image": "https://i.imgur.com/O5BMwNw.jpeg",
          "price": "6.8€",
          "products": [
            {
              "name": "Aceite de coco virgen extra Nat Sanno",
              "price": "4.50€"
            }
          ]
        },
        "lunch": {
          "name": "Pizza casera con base de tomate",
          "image": "https://i.imgur.com/OgBifvs.jpeg",
          "price": "5.4€",
          "products": [
            {
              "name": "Tomate frito Hacendado",
              "price": "0.68€"
            },
            {
              "name": "Orégano Hacendado",
              "price": "0.95€"
            }
          ]
        },
        "dinner": {
          "name": "Sopa de verduras con toque de aceite",
          "image": "https://i.imgur.com/aR7S2RS.jpeg",
          "price": "3.11€",
          "products": [
            {
              "name": "Aceite de oliva virgen extra Hacendado",
              "price": "2.35€"
            },
            {
              "name": "Sal rosa del Himalaya Hacendado",
              "price": "1.95€"
            }
          ]
        }
      }
  ]